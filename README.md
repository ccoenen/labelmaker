# Labelmaker

Take an spreadsheet from excel or LibreOffice and create a PDF for printing. Originally written to create a collection of address labels from a spreadsheet, but can be extended in various ways.


## Usage

Have a spreadsheet (`.ods` or `.xlsx`) ready, where the first worksheet has Columns "Line 1", "Line 2" and "Line 3".

Run this command

`node index.js --preset Z54 my-spreadsheet.ods my-printable-document.pdf`

Take a look at `my-printable-document.pdf`. Currently some things need to be adjusted in code. In `lib/pdf-draw-label.js`, any single label will be constructed.


## Making your own presets

Put a file into the `presets` directory, and change the numbers. This should be fairly easy from the examples already in there. For now, only `mm` is implemented as a unit, because it is all I needed.
