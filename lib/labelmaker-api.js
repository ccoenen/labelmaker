import { parse } from './input-parser.js';
import { write } from './pdf-writer.js';

export function render(inputFile, outputFile, program) {
	const list = parse(inputFile);
	write(list, program.preset, outputFile);
}
