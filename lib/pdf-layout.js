import fs from 'fs';

import ini from 'ini';

// pdf-lib has all its dimensions in pt for some reason. 1pt == 2.834mm
// also, its axes work like in a mathematical coordinate system, not like an on-screen
// pixel based one. The origin (0,0) is at the BOTTOM left, and positive numbers go UPWARDS.

export let cols;
export let rows;
export let labelsPerSheet;
export let pageWidth;
export let pageHeight;
export let pageBorderLeft;
export let pageBorderTop;
export let labelWidth;
export let labelHeight;

export function mmToPt(mm) {
	return mm * 2.834;
}

export function setPreset(presetName) {
	const config = ini.parse(fs.readFileSync(`presets/${presetName}.conf`, 'utf-8'));
	let converter;
	switch (config.unit) {
	case 'mm':
		converter = mmToPt;
		break;
	case 'pt':
	case 'inch':
	default:
		throw `Unit ${config.unit} found in ${presetName}, this is not yet implemented`;
	}

	cols = parseInt(config.cols, 10);
	rows = parseInt(config.rows, 10);
	labelsPerSheet = cols * rows;

	pageWidth = converter(parseFloat(config.pageWidth));
	pageHeight = converter(parseFloat(config.pageHeight));
	pageBorderLeft = converter(parseFloat(config.pageBorderLeft));
	pageBorderTop = converter(parseFloat(config.pageBorderTop));
	labelWidth = converter(parseFloat(config.labelWidth));
	labelHeight = converter(parseFloat(config.labelHeight));
}

export function labelCoordinates(index) {
	const labelRow = Math.floor((index % labelsPerSheet) / cols);
	const labelColumn = (index % cols);
	return {
		x: pageBorderLeft + labelWidth * labelColumn,
		y: pageHeight - pageBorderTop - labelHeight * labelRow
	};
}
