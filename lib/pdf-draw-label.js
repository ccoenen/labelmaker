import { rgb } from 'pdf-lib';

import * as layout from './pdf-layout.js';

const DEBUG_RECTANGLE = false;

export function drawLabel(page, x, y, item) {
	const fromLineSize = 6;
	const lineSize = 13;
	const lineHeight = 16;

	if (DEBUG_RECTANGLE) {
		page.drawRectangle({
			borderColor: rgb(0.9, 0.9, 0.9),
			borderWidth: 1,
			x,
			y: y - layout.labelHeight,
			width: layout.labelWidth,
			height: layout.labelHeight
		});
	}

	// generally offsetting everything slightly to be in the middle of the label
	x = x + 10;
	y = y - 35;

	page.drawText('ohai!', {
		size: fromLineSize,
		x,
		y: y + 0
	});

	if (item['Line 1']) {
		page.drawText(item['Line 1'], {
			size: lineSize,
			x,
			y: y - lineHeight * 1
		});
	} else {
		console.warn(`Item does not have a "Line 1" Text: ${item}`);
	}

	if (item['Line 2']) {
		page.drawText(item['Line 2'], {
			size: lineSize,
			x,
			y: y - lineHeight * 2
		});
	} else {
		console.warn(`Item does not have a "Line 2" Text: ${item}`);
	}

	if (item['Line 3']) {
		page.drawText(item['Line 3'], {
			size: lineSize,
			x,
			y: y - lineHeight * 3
		});
	} else {
		console.warn(`Item does not have a "Line 3" Text: ${item}`);
	}
}
