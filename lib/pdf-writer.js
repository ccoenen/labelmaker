import fs from 'fs';

import { PDFDocument } from 'pdf-lib';

import * as layout from './pdf-layout.js';
import { drawLabel } from './pdf-draw-label.js';

export async function write(list, preset, filename) {
	const doc = await PDFDocument.create();
	let page;

	layout.setPreset(preset);

	list.forEach((element, index) => {
		if (index % layout.labelsPerSheet === 0) {
			page = doc.addPage([layout.pageWidth, layout.pageHeight]);
		}

		const {x, y} = layout.labelCoordinates(index);
		drawLabel(page, x, y, element);
	});

	fs.writeFileSync(filename, await doc.save());
}
