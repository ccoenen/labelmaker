import XLSX from 'xlsx';

export function parse(filename) {
	const workbook = XLSX.readFile(filename);
	const firstSheetName = workbook.SheetNames[0];
	const firstSheet = workbook.Sheets[firstSheetName];
	return XLSX.utils.sheet_to_json(firstSheet);
}
