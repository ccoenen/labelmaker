import { program } from 'commander';

import { render } from './lib/labelmaker-api.js';

program
	.version('0.0.1')
	.option('-p, --preset <preset-name>', 'which preset should be chosen', 'A4-address')
	.arguments('<input-list> <output-pdf>')
	.action(render)
	.description('rendering a list of items into a pdf, for example to create address stickers')
	.parse(process.argv);
